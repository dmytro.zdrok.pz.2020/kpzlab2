﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WPFLABtry2
{
    [Serializable]
    public class Order
    {
        public int ID { get; set; }
        public int CustomerId { get; set; }
        public int SalerId { get; set; }
        public int ProductId { get; set; }
        public string OrderTime { get; set; }
        public int Count { get; set; }
        public double TotalCost { get; set; }
    }
}
