﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WPFLABtry2
{
    [Serializable]
    public class Saler
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string PhoneNumber { get; set; }
        public string Birthday { get; set; }
    }
}
