﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WPFLABtry2
{
    [Serializable]
    public class Product
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int ProductType { get; set; }
        public int BrandID { get; set; }
    }
}
