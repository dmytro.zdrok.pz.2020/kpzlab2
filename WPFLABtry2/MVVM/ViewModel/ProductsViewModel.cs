﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFLABtry2.DB;

namespace WPFLABtry2.MVVM.ViewModel
{
    public class ProductsViewModel
    {
        public IEnumerable<Product> Products { get; set; }

        public ProductsViewModel()
        {
            Products = BoardGamesDB.GetProducts();
        }
    }
}
