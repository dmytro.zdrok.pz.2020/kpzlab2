﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFLABtry2.DB;

namespace WPFLABtry2.MVVM.ViewModel
{
    public class SalersViewModel
    {
        public IEnumerable<Saler> Salers { get; set; }
        public SalersViewModel()
        {
            Salers = BoardGamesDB.GetSalers();
        }
    }
}
