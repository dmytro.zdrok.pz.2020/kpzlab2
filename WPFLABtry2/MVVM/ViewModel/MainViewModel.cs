﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using WPFLABtry2.Core;

namespace WPFLABtry2.MVVM.ViewModel
{
    class MainViewModel : ObservableObject
    {
        public RelayCommand SalersViewCommand { get; set; }
        public RelayCommand CustomersViewCommand { get; set; }
        public RelayCommand ProductsViewCommand { get; set; }
        public RelayCommand OrdersViewCommand { get; set; }

        private object _currentView;

        public SalersViewModel SalersVM { get; set; }
        public CustomersViewModel CustomersVM { get; set; }
        public ProductsViewModel ProductsVM { get; set; }
        public OrdersViewModel OrdersVM { get; set; }
        public object CurrentView
        {
            get { return _currentView; }
            set 
            { 
                _currentView = value;
                OnPropertyChanged();
            }
        }


        public MainViewModel()
        {
            SalersVM = new SalersViewModel();
            CustomersVM = new CustomersViewModel();
            ProductsVM = new ProductsViewModel();
            OrdersVM = new OrdersViewModel();
            CurrentView = SalersVM;

            SalersViewCommand = new RelayCommand(x => 
            {
                CurrentView = SalersVM;
            });

            CustomersViewCommand = new RelayCommand(x =>
            {
                CurrentView = CustomersVM;
            });

            ProductsViewCommand = new RelayCommand(x =>
            {
                CurrentView = ProductsVM;
            });

            OrdersViewCommand = new RelayCommand(x =>
            {
                CurrentView = OrdersVM;
            });
        }
    }
}
