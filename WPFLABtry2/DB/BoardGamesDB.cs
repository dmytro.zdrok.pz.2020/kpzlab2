﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFLABtry2.DB
{
    public class BoardGamesDB
    {
        static SqlConnection conn = new SqlConnection("Data Source=LAPTOP-R88STM5T;Initial Catalog=BoardGamesDB;Integrated Security=True;TrustServerCertificate=True");

        public static List<Saler> GetSalers()
        {
            List<Saler> salers = new List<Saler>();
            conn.Open();
            string qry = "select * from Salers";
            SqlCommand sqlCommand = new SqlCommand(qry,conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
            {
                salers.Add(new Saler()
                {
                    ID = (int)sqlDataReader["saler_id"],
                    Name = sqlDataReader["name"].ToString(),
                    Surname = sqlDataReader["surname"].ToString(),
                    Birthday = sqlDataReader["birthday"].ToString(),
                    PhoneNumber = sqlDataReader["phone_number"].ToString()
                });
            }
            conn.Close();
            return salers;
        }

        public static List<Customer> GetCustomers()
        {
            List<Customer> customers = new List<Customer>();
            conn.Open();
            string qry = "select * from Costumers";
            SqlCommand sqlCommand = new SqlCommand(qry, conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
            {
                customers.Add(new Customer()
                {
                    ID = (int)sqlDataReader["customer_id"],
                    Name = sqlDataReader["name"].ToString(),
                    Surname = sqlDataReader["surname"].ToString(),
                    Birthday = sqlDataReader["birthday"].ToString(),
                    PhoneNumber = sqlDataReader["phone_number"].ToString()
                });
            }
            conn.Close();
            return customers;
        }

        public static List<Product> GetProducts()
        {
            List<Product> products = new List<Product>();
            conn.Open();
            string qry = "select * from Products";
            SqlCommand sqlCommand = new SqlCommand(qry, conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
            {
                products.Add(new Product()
                {
                    ID = (int)sqlDataReader["product_id"],
                    Name = sqlDataReader["name"].ToString(),
                    BrandID = (int)sqlDataReader["brand_id"],
                    Price = Convert.ToDouble(sqlDataReader["price"]),
                    ProductType = (int)sqlDataReader["product_type_id"]
                });
            }
            conn.Close();
            return products;
        }

        public static List<Order> GetOrders()
        {
            List<Order> orders = new List<Order>();
            conn.Open();
            string qry = "select * from Orders";
            SqlCommand sqlCommand = new SqlCommand(qry, conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
            {
                orders.Add(new Order()
                {
                    ID = (int)sqlDataReader["order_id"],
                    CustomerId = (int)sqlDataReader["customer_id"],
                    SalerId = (int)sqlDataReader["saler_id"],
                    ProductId = (int)sqlDataReader["product_id"],
                    OrderTime = sqlDataReader["order_time"].ToString(),
                    TotalCost = Convert.ToDouble(sqlDataReader["total_cost"]),
                    Count = (int)sqlDataReader["count"]
                });
            }
            conn.Close();
            return orders;
        }
    }
}
